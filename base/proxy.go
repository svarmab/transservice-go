package base

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"

	consulsd "github.com/go-kit/kit/sd/consul"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/lb"
	kithttp "github.com/go-kit/kit/transport/http"
	"sps/transservice-go/model"
)

//ProxyConfig the config for our proxies
type ProxyConfig struct {
	Instancer              *consulsd.Instancer
	Path                   string
	Protocol               string
	MaxAttempts            int
	Method                 string
	MaxTime                time.Duration
	CreditReportCheckURL   *url.URL
	CriminalReportCheckURL *url.URL
	GetHireAPITokenURL     *url.URL
}

//ProxyService an interface to define proxy calls
type ProxyService interface {
	CreditReportCheck(ctx context.Context, req model.SampleRequest) (model.SampleResponse, error)
	CriminalReportCheck(ctx context.Context, req model.SampleRequest) (model.SampleResponse, error)
	GetHireAPIToken(ctx context.Context, req model.TokenRequest) (model.TokenResponse, error)
}

type proxyService struct {
	context                     context.Context
	creditReportCheckEndpoint   endpoint.Endpoint
	criminalReportCheckEndpoint endpoint.Endpoint
	getHireAPITokenEndpoint     endpoint.Endpoint
	ProxyService
}

func (is proxyService) CreditReportCheck(ctx context.Context, req model.SampleRequest) (model.SampleResponse, error) {
	response, err := is.creditReportCheckEndpoint(ctx, req)
	if err != nil {
		return model.SampleResponse{}, err
	}
	res, ok := response.(model.SampleResponse)
	if !ok {
		return model.SampleResponse{}, errors.New("invalid response")
	}
	return res, nil
}

func (is proxyService) CriminalReportCheck(ctx context.Context, req model.SampleRequest) (model.SampleResponse, error) {
	response, err := is.criminalReportCheckEndpoint(ctx, req)
	if err != nil {
		return model.SampleResponse{}, err
	}
	res, ok := response.(model.SampleResponse)
	if !ok {
		return model.SampleResponse{}, errors.New("invalid response")
	}
	return res, nil
}

func (is proxyService) GetHireAPIToken(ctx context.Context, req model.TokenRequest) (model.TokenResponse, error) {
	fmt.Println("inside GetHireAPIToken :: req=", req)
	response, err := is.getHireAPITokenEndpoint(ctx, req)
	if err != nil {
		return model.TokenResponse{}, err
	}
	res, ok := response.(model.TokenResponse)
	if !ok {
		return model.TokenResponse{}, errors.New("invalid response")
	}
	return res, nil
}

//ProxyMiddleware middleware definition for our proxy service
type ProxyMiddleware func(is ProxyService) ProxyService

//NewProxyMiddleware ...
func NewProxyMiddleware(ctx context.Context, config ProxyConfig, log log.Logger) ProxyMiddleware {
	creditReportCheck := MakeProxyEndPoints(http.MethodPost, config.CreditReportCheckURL, config, encodeCreditReportCheckRequest, decodeCreditReportCheckResp, log)
	criminalReportCheck := MakeProxyEndPoints(http.MethodPost, config.CriminalReportCheckURL, config, encodeCriminalReportCheckRequest, decodeCriminalReportCheckResp, log)
	getHireToken := MakeProxyEndPoints(http.MethodPost, config.GetHireAPITokenURL, config, encodeTokenRequest, decodeTokenResp, log)

	return func(next ProxyService) ProxyService {
		return proxyService{
			context:                     ctx,
			creditReportCheckEndpoint:   creditReportCheck,
			criminalReportCheckEndpoint: criminalReportCheck,
			getHireAPITokenEndpoint:     getHireToken,
		}
	}
}

//MakeProxyEndPoints makes the proxy endpoints
func MakeProxyEndPoints(method string, path *url.URL, config ProxyConfig, encoder kithttp.EncodeRequestFunc, decoder kithttp.DecodeResponseFunc, log log.Logger) endpoint.Endpoint {
	var endpointer sd.FixedEndpointer
	var e endpoint.Endpoint
	e = kithttp.NewClient(
		method,
		path,
		encoder,
		decoder,
	).Endpoint()
	endpointer = append(endpointer, e)
	balancer := lb.NewRoundRobin(endpointer)
	return lb.Retry(config.MaxAttempts, config.MaxTime, balancer)
}

func encodeCreditReportCheckRequest(ctx context.Context, r *http.Request, req interface{}) error {
	setRequestHeaders(ctx, r, req)
	request, ok := req.(model.SampleRequest)
	if !ok {
		return errors.New("Bad Request")
	}
	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(request); err != nil {
		return err
	}
	r.Body = ioutil.NopCloser(&buf)
	return nil
}

func encodeCriminalReportCheckRequest(ctx context.Context, r *http.Request, req interface{}) error {
	setRequestHeaders(ctx, r, req)
	request, ok := req.(model.SampleRequest)
	if !ok {
		return errors.New("Bad Request")
	}
	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(request); err != nil {
		return err
	}
	r.Body = ioutil.NopCloser(&buf)
	return nil
}

func encodeTokenRequest(ctx context.Context, r *http.Request, req interface{}) error {
	setRequestHeaders(ctx, r, req)
	request, ok := req.(model.TokenRequest)
	if !ok {
		return errors.New("Bad Request")
	}
	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(request); err != nil {
		return err
	}
	r.Body = ioutil.NopCloser(&buf)
	fmt.Println("r=", r)
	return nil
}

func decodeCreditReportCheckResp(ctx context.Context, r *http.Response) (interface{}, error) {
	var res model.SampleResponse
	if r.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("Status code incorrect. Expected: %v received %v for CreditReportCheck.", http.StatusOK, r.StatusCode)
	}
	err := json.NewDecoder(r.Body).Decode(&res)
	return res, err
}

func decodeCriminalReportCheckResp(ctx context.Context, r *http.Response) (interface{}, error) {
	var res model.SampleResponse
	if r.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("Status code incorrect. Expected: %v received %v for CriminalReportCheck.", http.StatusOK, r.StatusCode)
	}
	err := json.NewDecoder(r.Body).Decode(&res)
	return res, err
}

func decodeTokenResp(ctx context.Context, r *http.Response) (interface{}, error) {
	var res model.TokenResponse
	fmt.Println("r.StatusCode=", r.StatusCode)
	if r.StatusCode != http.StatusCreated {
		return nil, fmt.Errorf("Status code incorrect. Expected: %v received %v from token respose.", http.StatusOK, r.StatusCode)
	}
	err := json.NewDecoder(r.Body).Decode(&res)
	return res, err
}
