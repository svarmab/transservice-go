package base

import (
	"context"
	"fmt"
	"net/http"

	"github.com/go-kit/kit/log"

	"sps/transservice-go/api"
	mydb "sps/transservice-go/db"
	"sps/transservice-go/model"
)

// Service ...
type Service interface {
	Check(ctx context.Context) (bool, error)
	GetCreditReport(ctx context.Context, req model.SampleRequest) (model.SampleResponse, error)
}

type baseService struct {
	log          log.Logger
	config       model.Configuration
	database     mydb.DB
	proxyService ProxyService
}

//NewService ...
func NewService(log log.Logger, config model.Configuration, dbase mydb.DB, proxyService ProxyService) Service {
	return baseService{
		log:          log,
		config:       config,
		database:     dbase,
		proxyService: proxyService,
	}
}

func (s baseService) Check(ctx context.Context) (bool, error) {
	return true, nil
}

func (s baseService) GetCreditReport(ctx context.Context, request model.SampleRequest) (model.SampleResponse, error) {
	//do request business validations and call outbound apis here
	isValid := api.IsVaidGetCreditReportReq(request)
	if !isValid {
		return model.SampleResponse{}, fmt.Errorf("bad request")
	}

	// get token sample call
	tokenresp, err := s.proxyService.GetHireAPIToken(ctx, model.TokenRequest{
		ClientId: s.config.Outbound.HiresClientID,
		ApiKey:   s.config.Outbound.HiresAPIKey,
	})

	if err != nil {
		fmt.Println("err in token call=", err)
	}

	fmt.Println("response from get token call=", tokenresp)

	return model.SampleResponse{StatusCode: http.StatusOK, Message: tokenresp.Token}, nil
}
