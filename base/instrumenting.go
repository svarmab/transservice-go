package base

import (
	"context"
	"strconv"
	"time"

	"github.com/go-kit/kit/metrics"
	"sps/transservice-go/model"
)

// Instrumenting ...
type Instrumenting func(Service) Service

type instrumenter struct {
	labelNames            []string
	requestCount          metrics.Counter
	errCount              metrics.Counter
	requestLatency        metrics.Histogram
	requestLatencySummary metrics.Histogram
	counts                metrics.Gauge
}
type instrumentingService struct {
	instrumenter
	next Service
}

//NewInstrumentingService ...
func NewInstrumentingService(labelNames []string, counter, errCounter metrics.Counter, latency metrics.Histogram, histogram metrics.Histogram) Instrumenting {
	return func(next Service) Service {
		return instrumentingService{
			instrumenter{
				labelNames:            labelNames,
				requestCount:          counter,
				errCount:              errCounter,
				requestLatencySummary: latency,
				requestLatency:        histogram,
			},
			next,
		}
	}
}

func (s instrumenter) instrument(begin time.Time, methodName string, code string, err error) {
	if len(s.labelNames) > 1 {
		s.requestCount.With(s.labelNames[0], methodName, s.labelNames[1], code).Add(1)
		s.requestLatency.With(s.labelNames[0], methodName, s.labelNames[1], code).Observe(time.Since(begin).Seconds())
		s.requestLatencySummary.With(s.labelNames[0], methodName, s.labelNames[1], code).Observe(time.Since(begin).Seconds())
		if err != nil {
			s.errCount.With(s.labelNames[0], methodName, s.labelNames[1], code).Add(1)
		}
	}
}

func (s instrumentingService) Check(ctx context.Context) (b bool, err error) {
	defer func(begin time.Time) {
		s.instrument(begin, "Check", "", err)
	}(time.Now())
	return s.next.Check(ctx)
}

func (s instrumentingService) GetCreditReport(ctx context.Context, req model.SampleRequest) (resp model.SampleResponse, err error) {
	defer func(begin time.Time) {
		s.instrument(begin, "GetCreditReport", strconv.Itoa(ErrStatusCode(err)), err)
	}(time.Now())
	return s.next.GetCreditReport(ctx, req)
}

//ProxyInstrumentingService ...
type ProxyInstrumentingService func(s ProxyService) ProxyService

type proxyInstrumentingService struct {
	instrumenting instrumenter
	next          ProxyService
}

//NewProxyInstrumentingService ...
func NewProxyInstrumentingService(labelNames []string, reqCount, errCount metrics.Counter, reqLatencySummary, reqLatency metrics.Histogram) ProxyInstrumentingService {
	return func(next ProxyService) ProxyService {
		return &proxyInstrumentingService{
			instrumenting: instrumenter{
				labelNames:            labelNames,
				requestCount:          reqCount,
				errCount:              errCount,
				requestLatencySummary: reqLatencySummary,
				requestLatency:        reqLatency,
			},
			next: next,
		}
	}
}

//CreditReportCheck ...
func (is proxyInstrumentingService) CreditReportCheck(ctx context.Context, req model.SampleRequest) (res model.SampleResponse, err error) {
	defer func(begin time.Time) {
		is.instrumenting.instrument(begin, "CreditReportCheck", strconv.Itoa(ErrStatusCode(err)), err)
	}(time.Now())
	return is.next.CreditReportCheck(ctx, req)
}

//CriminalReportCheck ...
func (is proxyInstrumentingService) CriminalReportCheck(ctx context.Context, req model.SampleRequest) (res model.SampleResponse, err error) {
	defer func(begin time.Time) {
		is.instrumenting.instrument(begin, "CriminalReportCheck", strconv.Itoa(ErrStatusCode(err)), err)
	}(time.Now())
	return is.next.CriminalReportCheck(ctx, req)
}

//GetHireAPIToken ...
func (is proxyInstrumentingService) GetHireAPIToken(ctx context.Context, req model.TokenRequest) (res model.TokenResponse, err error) {
	defer func(begin time.Time) {
		is.instrumenting.instrument(begin, "GetHireAPIToken", strconv.Itoa(ErrStatusCode(err)), err)
	}(time.Now())
	return is.next.GetHireAPIToken(ctx, req)
}
