module sps/transservice-go

go 1.16

require (
	github.com/flimzy/diff v0.1.7 // indirect
	github.com/flimzy/testy v0.1.17 // indirect
	github.com/go-kit/kit v0.12.0
	github.com/go-kivik/couchdb v2.0.0+incompatible // indirect
	github.com/go-kivik/kivik v2.0.0+incompatible // indirect
	github.com/go-kivik/kiviktest v2.0.0+incompatible // indirect
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/hashicorp/consul/api v1.12.0
	github.com/lib/pq v1.10.4
	github.com/prometheus/client_golang v1.11.0
	gitlab.com/flimzy/testy v0.8.0 // indirect
)
