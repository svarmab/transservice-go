package base

import (
	"context"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/gorilla/handlers"
	"sps/transservice-go/model"
)

//Middleware middleware func
type Middleware func(Service) Service

//NewLoggingMiddleware ...
func NewLoggingMiddleware(logger log.Logger) Middleware {
	return func(next Service) Service {
		return &loggingMiddleware{
			next:   next,
			logger: logger,
		}
	}
}

type loggingMiddleware struct {
	next   Service
	logger log.Logger
}

//NewPanicLogger returns the panic logger
func NewPanicLogger(logger log.Logger) handlers.RecoveryHandlerLogger {
	return panicLogger{
		logger,
	}
}

type panicLogger struct {
	log.Logger
}

func (pl panicLogger) Println(messages ...interface{}) {
	for _, msg := range messages {
		pl.Log("panic ", msg)
	}
}

func (mw loggingMiddleware) Check(ctx context.Context) (res bool, err error) {
	defer func(begin time.Time) {
		if err != nil {
			mw.logger.Log("method", "Check", "took", time.Since(begin), "err", err, "xff", xff(ctx))
		}
	}(time.Now())
	return mw.next.Check(ctx)
}

func (mw loggingMiddleware) GetCreditReport(ctx context.Context, request model.SampleRequest) (resp model.SampleResponse, err error) {
	defer func(begin time.Time) {
		if err != nil {
			mw.logger.Log("method", "GetCreditReport", "took", time.Since(begin), "err", err, "xff", xff(ctx))
		}
	}(time.Now())
	return mw.next.GetCreditReport(ctx, request)
}

//ProxyLoggingMiddleware ...
type ProxyLoggingMiddleware func(service ProxyService) ProxyService

//proxyLoggingMiddelware ...
type proxyLoggingMiddelware struct {
	next   ProxyService
	logger log.Logger
}

//NewProxyLoggingMiddelware ...
func NewProxyLoggingMiddelware(logger log.Logger) ProxyMiddleware {
	return func(service ProxyService) ProxyService {
		return &proxyLoggingMiddelware{
			next:   service,
			logger: logger,
		}
	}
}

//CreditReportCheck ...
func (mw proxyLoggingMiddelware) CreditReportCheck(ctx context.Context, req model.SampleRequest) (res model.SampleResponse, err error) {
	defer func(begin time.Time) {
		if err != nil {
			mw.logger.Log("method", "CreditReportCheck", "took", time.Since(begin), "err", err)
		}
	}(time.Now())
	return mw.next.CreditReportCheck(ctx, req)
}

//CriminalReportCheck ...
func (mw proxyLoggingMiddelware) CriminalReportCheck(ctx context.Context, req model.SampleRequest) (res model.SampleResponse, err error) {
	defer func(begin time.Time) {
		if err != nil {
			mw.logger.Log("method", "CriminalReportCheck", "took", time.Since(begin), "err", err)
		}
	}(time.Now())
	return mw.next.CriminalReportCheck(ctx, req)
}

//GetHireAPIToken ...
func (mw proxyLoggingMiddelware) GetHireAPIToken(ctx context.Context, req model.TokenRequest) (res model.TokenResponse, err error) {
	defer func(begin time.Time) {
		if err != nil {
			mw.logger.Log("method", "GetHireAPIToken", "took", time.Since(begin), "err", err)
		}
	}(time.Now())
	return mw.next.GetHireAPIToken(ctx, req)
}
