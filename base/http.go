package base

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"

	transhttp "github.com/go-kit/kit/transport/http"

	"github.com/go-kit/kit/log"
	httptransport "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"sps/transservice-go/model"
)

var (
	// ErrNotFound ...
	ErrNotFound = errors.New("not found")
	// ErrBadRequest ...
	ErrBadRequest = errors.New("bad request")
	// ErrInternalServerError ...
	ErrInternalServerError = errors.New("Internal Server Error")
	//ErrDBUnavailable ...
	ErrDBUnavailable = errors.New("Service unhealthy")
)

type errorer interface {
	error() error
}

func decodeRequest(ctx context.Context, req *http.Request) (r interface{}, err error) {
	var request = model.SampleRequest{}
	if err = json.NewDecoder(req.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func encodeError(_ context.Context, err error, w http.ResponseWriter) {
	if err == nil {
		panic("encodeError with nil error")
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusInternalServerError)
	json.NewEncoder(w).Encode(map[string]interface{}{
		"error": err.Error(),
	})
}

//MakeHTTPHandler makes the http handler and returns it
func MakeHTTPHandler(s Service, logger log.Logger, version string, basePath string) http.Handler {
	r := mux.NewRouter()
	e := MakeServerEndpoints(s)
	baseRoute := "/" + basePath + "/" + version

	r.Methods(http.MethodGet).Path("/healthcheck").Handler(httptransport.NewServer(
		e.Check,
		httptransport.NopRequestDecoder,
		encodeHealthResponse,
	))

	r.Methods(http.MethodPost).Path(baseRoute + "/getcreditreport").Handler(httptransport.NewServer(
		e.GetCreditReport,
		decodeRequest,
		encodeResponse,
		httptransport.ServerErrorEncoder(ErrorEncoder),
		httptransport.ServerBefore(httptransport.PopulateRequestContext),
	))

	return r
}

func encodeHealthResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(errorer); ok && e.error() != nil {
		encodeError(ctx, e.error(), w)
		return nil
	}
	val, ok := response.(bool)
	if ok && !val {
		w.WriteHeader(http.StatusTooManyRequests)
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}

// encodeResponse is the common method to encode all response types to the
// client. I chose to do it this way because, since we're using JSON, there's no
// reason to provide anything more specific. It's certainly possible to
// specialize on a per-response (per-method) basis.
func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(errorer); ok && e.error() != nil {
		// Not a Go kit transport error, but a business-logic error.
		// Provide those as HTTP errors.
		encodeError(ctx, e.error(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}

// ErrorEncoder ...
func ErrorEncoder(_ context.Context, err error, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	switch err {
	case
		ErrBadRequest:
		err, _ := json.Marshal(model.Error{
			ErrorCode:    "SER-CPREFS-INPUT_INVALID",
			ErrorMessage: err.Error(),
		})
		w.WriteHeader(http.StatusBadRequest)
		w.Write(err)
		break

	default:
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func xff(ctx context.Context) string {
	xff, _ := ctx.Value(transhttp.ContextKeyRequestXForwardedFor).(string)
	return xff
}

func setRequestHeaders(ctx context.Context, r *http.Request, req interface{}) error {
	r.Header.Set("Content-Type", "application/json")
	return nil
}

func ErrStatusCode(err error) int {
	code := http.StatusInternalServerError
	switch err {
	case nil:
		code = http.StatusOK
		break
	case ErrBadRequest:
		code = http.StatusBadRequest
		break
	case ErrNotFound:
		code = http.StatusNotFound
		break
	case ErrInternalServerError:
		code = http.StatusInternalServerError
		break
	case ErrDBUnavailable:
		code = http.StatusServiceUnavailable
		break
	default:
		code = http.StatusInternalServerError
		break
	}
	return code
}
