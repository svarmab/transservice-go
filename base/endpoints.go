package base

import (
	"context"
	"errors"

	"github.com/go-kit/kit/endpoint"
	"sps/transservice-go/model"
)

var (
	errInvalidRequest = errors.New("Invalid Request")
)

// Endpoints ...
type Endpoints struct {
	Check           endpoint.Endpoint
	GetCreditReport endpoint.Endpoint
}

// MakeServerEndpoints ...
func MakeServerEndpoints(s Service) Endpoints {
	return Endpoints{
		Check:           MakeCheck(s),
		GetCreditReport: MakeGetCreditReport(s),
	}
}

// MakeCheck ...
func MakeCheck(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		return s.Check(ctx)
	}
}

// MakeGetCreditReport ...
func MakeGetCreditReport(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		r, ok := request.(model.SampleRequest)
		if !ok {
			return nil, errInvalidRequest
		}
		return s.GetCreditReport(ctx, r)
	}
}
