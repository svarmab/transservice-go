package main

import (
	"context"
	"flag"
	"fmt"
	"log/syslog"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/metrics/prometheus"
	"github.com/gorilla/handlers"

	stdprometheus "github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"sps/transservice-go/base"
	mydb "sps/transservice-go/db"
	"sps/transservice-go/model"
)

func main() {
	var (
		serviceName   = flag.String("service.name", "transservice", "name of the microservice")
		serviceGroup  = flag.String("service.group", "transservice", "service group")
		basePath      = flag.String("service.base.path", "transservice", "Name of microservice")
		version       = flag.String("service.version", "v1", "Version of microservice (Default v1)")
		httpAddr      = flag.String("http.addr", "localhost", "This is the addr at which http requests are accepted (Default localhost)")
		httpPort      = flag.Int("http.port", 8080, "This is the port at which http requests are accepted (Default :8080)")
		metricsPort   = flag.Int("metrics.port", 8082, "HTTP metrics listen address (Default 8082)")
		dataType      = flag.String("service.datatype", "test", "default Test")
		serverTimeout = flag.Int64("service.timeout", 5000, "service timeout in milliseconds")
		sysLogAddress = flag.String("syslog.address", "localhost:8081", "default location for the syslogger")
		maxAttempts   = flag.Int("outbound.service.attempts", 1, "max attempts for API")
		apiMaxTime    = flag.Int("outbound.service.maxtime", 10000, "maxTime for API in milliseconds")
	)
	flag.Parse()

	sysLogger, err := syslog.Dial("udp", *sysLogAddress, syslog.LOG_EMERG|syslog.LOG_LOCAL6, *serviceName)
	if err != nil {
		fmt.Printf("exit: %v\n", err)
		return
	}
	defer sysLogger.Close()

	var logger log.Logger

	{
		logger = log.NewJSONLogger(sysLogger)
		logger = log.With(logger, "serviceName", *serviceName)
		logger = log.With(logger, "ip", *httpAddr)
		logger = log.With(logger, "ts", log.DefaultTimestampUTC)
		logger = log.With(logger, "caller", log.DefaultCaller)
	}

	exit := func(err error, errHTTPServer, errMetricsServer error) {
		logger.Log("exit", err, "httpErr", errHTTPServer, "metricsErr", errMetricsServer)
	}
	errs := make(chan error)

	configVals, err := model.NewConfig("resources/config.json", *dataType)
	if err != nil {
		fmt.Println("failed to load config values", err)
	}

	dbase, err := mydb.InitDB(configVals.DBConfig)
	if err != nil {
		logger.Log("error while initialing the database for the dataType=%s Error=%s", dataType, err)
		return
	}
	defer closedb(dbase)

	creditReportCheckURL, err := url.Parse(configVals.Outbound.CreditReportURL)
	if err != nil {
		logger.Log("error parsing all creditReportCheck proxy URL", err)
		return
	}

	criminalReportCheckURL, err := url.Parse(configVals.Outbound.CriminalReportURL)
	if err != nil {
		logger.Log("error parsing all criminalReportCheck proxy URL", err)
		return
	}

	getHireTokenURL, err := url.Parse(configVals.Outbound.HiresAPITokenGenURL)
	if err != nil {
		logger.Log("error parsing all getHireToken proxy URL", err)
		return
	}
	//outbound Setup

	labelNames := []string{"method", "code"}
	constLabels := map[string]string{"serviceName": *basePath, "serviceGroup": *serviceGroup, "version": *version, "dataType": *dataType}

	errCounter := prometheus.NewCounterFrom(stdprometheus.CounterOpts{
		Name:        "err_count",
		Subsystem:   "outbound",
		Help:        "Number of errors.",
		ConstLabels: constLabels,
	}, labelNames)
	requestLatencyCounter := prometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
		Name:        "request_latency_seconds",
		Subsystem:   "outbound",
		Help:        "Total duration of requests in seconds.",
		ConstLabels: constLabels,
	}, labelNames)
	requestCounter := prometheus.NewCounterFrom(stdprometheus.CounterOpts{
		Name:        "request_count",
		Subsystem:   "outbound",
		Help:        "Number of requests received.",
		ConstLabels: constLabels,
	}, labelNames)
	requestLatencyHistogram := prometheus.NewHistogramFrom(stdprometheus.HistogramOpts{
		Name:        "request_latency",
		Subsystem:   "outbound",
		Help:        "Duration of request in seconds",
		Buckets:     []float64{.01, .025, .050, .1, .3, .6, 1},
		ConstLabels: constLabels,
	}, labelNames)

	var proxyService base.ProxyService
	proxyService = base.NewProxyMiddleware(
		context.Background(),
		base.ProxyConfig{
			MaxAttempts:            *maxAttempts,
			Method:                 http.MethodPost,
			MaxTime:                time.Duration(*apiMaxTime) * time.Millisecond,
			CreditReportCheckURL:   creditReportCheckURL,
			CriminalReportCheckURL: criminalReportCheckURL,
			GetHireAPITokenURL:     getHireTokenURL,
		}, logger)(proxyService)
	proxyService = base.NewProxyLoggingMiddelware(logger)(proxyService)
	proxyService = base.NewProxyInstrumentingService(labelNames, requestCounter, errCounter, requestLatencyCounter, requestLatencyHistogram)(proxyService)

	var s base.Service
	{
		s = base.NewService(logger, configVals, dbase, proxyService)
		s = base.NewLoggingMiddleware(logger)(s)

		labelNames := []string{"method"}
		constLabels := map[string]string{"serviceName": *basePath, "serviceGroup": *serviceGroup, "version": *version, "dataType": *dataType}

		s = base.NewInstrumentingService(labelNames,
			prometheus.NewCounterFrom(stdprometheus.CounterOpts{
				Name:        "request_count",
				Help:        "Number of requests received.",
				ConstLabels: constLabels,
			}, labelNames),
			prometheus.NewCounterFrom(stdprometheus.CounterOpts{
				Name:        "err_count",
				Help:        "Number of errors.",
				ConstLabels: constLabels,
			}, labelNames),
			prometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
				Name: "request_latency_microseconds",
				Help: "Total duration of requests in microseconds.",
			}, labelNames),
			prometheus.NewHistogramFrom(stdprometheus.HistogramOpts{
				Name:        "request_latency",
				Help:        "Duration of request in microseconds",
				Buckets:     []float64{.01, .025, .05, .1, .3, .6, 1},
				ConstLabels: constLabels,
			}, labelNames))(s)
	}

	h := base.MakeHTTPHandler(s, logger, *version, *basePath)
	h = http.TimeoutHandler(h, time.Duration(*serverTimeout)*time.Millisecond, "")

	httpServer := http.Server{
		Addr:    ":" + strconv.Itoa(*httpPort),
		Handler: handlers.RecoveryHandler(handlers.RecoveryLogger(base.NewPanicLogger(logger)))(h),
	}

	go func() {
		errs <- httpServer.ListenAndServe()
	}()

	metricsServer := http.Server{
		Addr:    ":" + strconv.Itoa(*metricsPort),
		Handler: promhttp.Handler(),
	}
	go func() {
		logger.Log("transport", "HTTP", "addr", *metricsPort)
		errs <- metricsServer.ListenAndServe()
	}()

	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errs <- fmt.Errorf("%s", <-c)
	}()

	error := <-errs
	errMetricsServer := metricsServer.Shutdown(context.Background())
	errHTTPServer := httpServer.Shutdown(context.Background())
	exit(error, errMetricsServer, errHTTPServer)
}

func closedb(dbase mydb.DB) {
	var emptydb mydb.DB
	if dbase != emptydb {
		dbase.Close()
	}
}
