package model

type SampleRequest struct {
}

type SampleResponse struct {
	StatusCode int    `json:"status"`
	Message    string `json:"message"`
}

// Error ...
type Error struct {
	ErrorCode    string `json:"errorCode,omitempty"`
	ErrorMessage string `json:"errorMessage,omitempty"`
}

// TokenRequest ...
type TokenRequest struct {
	ClientId string `json:"clientId,omitempty"`
	ApiKey   string `json:"apiKey,omitempty"`
}

// TokenResponse ...
type TokenResponse struct {
	Token   string `json:"token,omitempty"`
	Expires string `json:"expires,omitempty"`
}
