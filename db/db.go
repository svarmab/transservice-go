package db

import (
	dbsql "database/sql"
	"fmt"
	_ "github.com/lib/pq"

	"sps/transservice-go/model"
)

// DB ...
type DB struct {
	*dbsql.DB
}

//InitDB ...
func InitDB(dbconfig model.DBConfig) (DB, error) {
	psqlconn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", dbconfig.Host, dbconfig.Port, dbconfig.User, dbconfig.Password, dbconfig.DBName)
	db, err := dbsql.Open("postgres", psqlconn)
	if err != nil {
		return DB{}, err
	}
	// check db
	err = db.Ping()
	fmt.Println("ping error =", err)

	return DB{db}, nil
}
