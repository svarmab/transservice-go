package model

import (
	"encoding/json"
	"io/ioutil"
	"strings"
	"time"
)

//Configs handles all configurations for all versions

//Config contains all of the different configs for a microservice (based on dataType)
type Config map[string]*Configuration

//Entry contains all of the different configs for a microservice for one dataType
type Configuration struct {
	DBConfig DBConfig `json:"db"`
	Outbound Outbound `json:"outbound"`
}

//App contains all of the common configs across microservices
type DBConfig struct {
	Host     string `json:"host"`
	Port     int    `json:"port"`
	User     string `json:"user"`
	Password string `json:"password"`
	DBName   string `json:"dbname"`
}

//Outbound contains this microservice's outbound configs
type Outbound struct {
	CreditReportURL          string        `json:"creditReportURL"`
	CreditReportURLTimeout   time.Duration `json:"creditReportTimeout"`
	CriminalReportURL        string        `json:"criminalReportURL"`
	CriminalReportURLTimeout time.Duration `json:"criminalReportTimeout"`
	HiresAPITokenGenURL      string        `json:"hiresAPITokenGenURL"`
	HiresAPITokenGenTimeout  time.Duration `json:"hiresAPITokenGenTimeout"`
	HiresClientID            string        `json:"hiresClientID"`
	HiresAPIKey              string        `json:"hiresAPIKey"`
}

//NewConfig takes the raw json and returns the complete config
func NewConfig(configFile, env string) (Configuration, error) {
	rawConfig, err := ioutil.ReadFile(configFile)
	if err != nil {
		return Configuration{}, err
	}
	c := make(Config)
	err = json.Unmarshal(rawConfig, &c)
	if err != nil {
		return Configuration{}, err
	}
	return *c[strings.ToLower(env)], nil
}
