package api

import (
  "testing"

  "sps/transservice-go/model"
)


func TestIsVaidGetCreditReportReq(t *testing.T) {
	tests := []struct {
		name string
		req  model.SampleRequest
		resp bool
	}{
		{name: "Happy_Path",
			req:  model.SampleRequest{},
			resp: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			resp := IsVaidGetCreditReportReq(tt.req)
			if resp != tt.resp {
				t.Fatalf("Failed at :: %v, expecting result :: %v, got :: %v", tt.name, tt.resp, resp)
			}
		})
	}
}
